---
title: CollegeGO
tags: ['Android']
---
App to facilitate a college going student. Includes timetable, attendance logging and exam and holidays reminder.
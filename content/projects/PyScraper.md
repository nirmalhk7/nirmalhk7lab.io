---
title: Silver-Scrapper
tags: ['Python'] 
---
I've tried making something I've always been fascinated with: web scrapers. I've followed [this tutorial for webscraping](https://www.tutorialspoint.com/python_web_scraping/index.htm) , and I'm making a complete web scrapper which I've baptised as Silver-Scrappy because I just did.
[Link](https://github.com/nirmalhk7/silver-scrappy)